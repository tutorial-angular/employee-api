package com.angular.project.employee.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Table;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Data
@Table(name = "employee")
public class Employee implements Comparable<Employee> {
    @Id
    @Column(name = "id_employee", columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id_employee;
    
    @Column(name = "name_employee")
    private String name_employee;
    
    @Column(name = "salary_employee")
    private Integer salary_employee;
    
    @Column(name = "age_employee")
    private Integer age_employee;
    
    public Employee() {
        
    }
    
    public Employee(String name_employee, Integer salary_employee, Integer age_employee){
        this.name_employee = name_employee;
        this.salary_employee = salary_employee;
        this.age_employee = age_employee;
    }
    
    @Override
    public int compareTo(Employee e) {
        return this.getId_employee().compareTo(e.getId_employee());
    }
}
