package com.angular.project.employee.controller;

import com.angular.project.employee.entity.Employee;
import com.angular.project.employee.repository.EmployeeRepository;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/employee")
public class EmployeeController {
    @Autowired
    private EmployeeRepository employeeRepository;
    
    @GetMapping("/getEmployees")
    public ResponseEntity<List<Employee>> getEmployees() {
        try {
            List<Employee> employees = new ArrayList<Employee>();
            employeeRepository.findAll().forEach(employees::add);

            if(employees.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            else{
                return new ResponseEntity<>(employees, HttpStatus.OK);
            }
        }
        catch(Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @GetMapping("/getEmployeeById/{id_employee}")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable("id_employee") Integer id_employee) {
        Optional<Employee> employeeData = employeeRepository.findById(id_employee);
        
        if(employeeData.isPresent()) {
            return new ResponseEntity<>(employeeData.get(), HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    
    @PostMapping("/addEmployee")
    public ResponseEntity<Employee> addEmployee(@Valid @RequestBody Employee employee) {
        if((!employee.getName_employee().isEmpty()) && employee.getSalary_employee() != null && employee.getAge_employee() != null) {
            try {
                Employee newEmployee = employeeRepository.save(new Employee(employee.getName_employee(),employee.getSalary_employee(),employee.getAge_employee()));
                return new ResponseEntity<>(newEmployee, HttpStatus.CREATED);
            }
            catch(Exception e) {
                return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        }
        else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
    
    @PutMapping("/editEmployee/{id_employee}")
    public ResponseEntity<Employee> editEmployee(@PathVariable("id_employee") Integer id_employee, @RequestBody Employee employee) {
        if((!employee.getName_employee().isEmpty()) && employee.getSalary_employee() != null && employee.getAge_employee() != null) {
            Optional<Employee> employeeData = employeeRepository.findById(id_employee);

            if(employeeData.isPresent()) {
                Employee editedEmployee = employeeData.get();
                editedEmployee.setName_employee(employee.getName_employee());
                editedEmployee.setSalary_employee(employee.getSalary_employee());
                editedEmployee.setAge_employee(employee.getAge_employee());
                return new ResponseEntity<>(employeeRepository.save(editedEmployee), HttpStatus.OK);
            }
            else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }
        else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
    
    @DeleteMapping("/deleteEmployee/{id_employee}")
    public ResponseEntity<HttpStatus> deleteEmployee(@PathVariable("id_employee") Integer id_employee) {
        try {
            employeeRepository.deleteById(id_employee);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch(Exception e) {
            return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
        }
    }
}
