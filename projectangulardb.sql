CREATE TABLE employee (
  id_employee SERIAL PRIMARY KEY,
  name_employee varchar,
  salary_employee int,
  age_employee int
);

INSERT INTO employee(name_employee, salary_employee, age_employee) VALUES ('Tiger Nixon',3200800,61);
INSERT INTO employee(name_employee, salary_employee, age_employee) VALUES ('Garrett Winters',1700750,63);
INSERT INTO employee(name_employee, salary_employee, age_employee) VALUES ('Ashton Cox',8600000,66);
INSERT INTO employee(name_employee, salary_employee, age_employee) VALUES ('Cedric Kelly',4330600,22);
INSERT INTO employee(name_employee, salary_employee, age_employee) VALUES ('Airi Satou',1627000,33);